package coffe_machine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
/**
* @author Erko Ensling
*	Reads external ingredients file and saves ingredients to external file.
*	.test() allows to run a test on reading writing to see if the reading writing is done OK.
*/
public class IngredientsReadWrite {
	static String file = "C:\\Users\\Erko Ensling\\workspace\\Homework\\src\\coffe_machine\\ingredients.txt";

	public static void main(String[] args) {
		if (Controls.debug) {
			test();
		}
	}

	private static void test() {
		// Test for reading and writing a ingredients file
		refill();
		try {
			write();
		} catch (IOException e) {
			System.out.println("ERROR WRITING" + file);
			e.printStackTrace();
		}
		// change the values back to initial
		Controls.milk = 0;
		Controls.cocoa = 0;
		Controls.coffee = 0;
		Controls.water = 0;
		Controls.sugar = 0;

		// read values stored in external file
		try {
			read();
		} catch (IOException e) {
			System.out.println("ERROR READING" + file);
			e.printStackTrace();
		}
		// print out the changed values to check what they are
		System.out.println(Controls.milk + "\n" + Controls.cocoa + "\n" + Controls.coffee + "\n" + Controls.water + "\n"
				+ Controls.sugar);
	}

	public static void refill() {
		Controls.milk = 5000; // set values to write
		Controls.cocoa = 500; // set values to write
		Controls.coffee = 1500; // set values to write
		Controls.water = 7000; // set values to write
		Controls.sugar = 2000; // set values to write

	}

	public static void write() throws IOException {
		if (Controls.debug) {
			System.out.println("#IngredientsReadWrite.Write started...");
		}

		// creates a FileWriter Object
		FileWriter writer = new FileWriter(file);
		// Writes the content to the file, with given syntax

		writer.write("<milk>\n" + Controls.milk + "\n</milk>\n");
		writer.write("<coffee>\n" + Controls.coffee + "\n</coffee>\n");
		writer.write("<water>\n" + Controls.water + "\n</water>\n");
		writer.write("<sugar>\n" + Controls.sugar + "\n</sugar>\n");
		writer.write("<cocoa>\n" + Controls.cocoa + "\n</cocoa>");
		writer.flush();
		writer.close();
		if (Controls.debug) {
			System.out.println("#IngredientsReadWrite.write OK!");
		}
	}

	public static void read() throws IOException {
		if (Controls.debug) {
			System.out.println(
					"#IngredientsReadWrite.read Started(ingridients as they are stored and read from the file...");
		}

		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			if (line.equals("<milk>")) {
				while ((line = br.readLine()) != null) {
					if (line.equals("</milk>"))
						break;
					if (Controls.debug) {
						System.out.println("	#milk: " + line);
					}
					Controls.milk = Double.parseDouble(line);
				}
			}
			if (line.equals("<coffee>")) {
				while ((line = br.readLine()) != null) {
					if (line.equals("</coffee>"))
						break;
					if (Controls.debug) {
						System.out.println("	#coffee: " + line);
					}
					Controls.coffee = Double.parseDouble(line);
				}
			}
			if (line.equals("<cocoa>")) {
				while ((line = br.readLine()) != null) {
					if (line.equals("</cocoa>"))
						break;
					if (Controls.debug) {
						System.out.println("	#cocoa:" + line); // test line to see if it gets,the right value.
					}
					Controls.cocoa = Double.parseDouble(line);
				}
			}
			if (line.equals("<sugar>")) {
				while ((line = br.readLine()) != null) {
					if (line.equals("</sugar>"))
						break;
					if (Controls.debug) {
						System.out.println("	#sugar:" + line);  // test line to see if it gets,the right value.
					}
					Controls.sugar = Double.parseDouble(line);
				}
			}
			if (line.equals("<water>")) {
				while ((line = br.readLine()) != null) {
					if (line.equals("</water>"))
						break;
					if (Controls.debug) {
						System.out.println("	#water:" + line);  // test line to see if it gets,the right value.
					}
					Controls.water = Double.parseDouble(line);
				}
			}
		}

		br.close();
		if (Controls.debug) {
			System.out.println("#IngredientsReadWrite.read OK");
		}
	}

}
