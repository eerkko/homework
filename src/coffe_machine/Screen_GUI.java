package coffe_machine;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.control.*;
import javafx.scene.text.*;
import javafx.stage.Stage;

public class Screen_GUI extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		int width = 610;
		int height = 800;

		stage.setTitle("Coffe Machine - Erko Ensling IA17");// Titel
		Controls.startUp();// console startup, in the background.
		Pane layout = new Pane();

		Rectangle screen = new Rectangle(500, 300); // screen background
		screen.setFill(Color.LIGHTGRAY);

		Text displayText = new Text("Select your\n  drink.");
		displayText.setFont(new Font(18));
		displayText.setTextAlignment(TextAlignment.CENTER);

		Text displayTextLeft = new Text(" \n" + Controls.drinks.get(0) + "\n \n \n" + Controls.drinks.get(1)
		+ "\n \n \n" + Controls.drinks.get(2) + "\n \n \n" + Controls.drinks.get(3) + "\n ");
		displayTextLeft.setFont(new Font(18));

		Text displayTextRight = new Text(" \n" + Controls.drinks.get(4) + "\n \n \n" + Controls.drinks.get(5)
		+ "\n \n \n" + Controls.drinks.get(6) + "\n \n \n" + Controls.drinks.get(7) + "\n ");
		displayTextRight.setFont(new Font(18));
		displayTextRight.setTextAlignment(TextAlignment.RIGHT);

		Button button1 = new Button("1");
		button1.setPrefSize(40, 40);
		button1.setLayoutX(10);
		button1.setLayoutY(40);

		Button button2 = new Button("2");
		button2.setPrefSize(40, 40);
		button2.setLayoutX(10);
		button2.setLayoutY(110);

		Button button3 = new Button("3");
		button3.setPrefSize(40, 40);
		button3.setLayoutX(10);
		button3.setLayoutY(180);

		Button button4 = new Button("4");
		button4.setPrefSize(40, 40);
		button4.setLayoutX(10);
		button4.setLayoutY(250);

		Button button5 = new Button("5");
		button5.setPrefSize(40, 40);
		button5.setLayoutX(560);
		button5.setLayoutY(40);

		Button button6 = new Button("6");
		button6.setPrefSize(40, 40);
		button6.setLayoutX(560);
		button6.setLayoutY(110);

		Button button7 = new Button("7");
		button7.setPrefSize(40, 40);
		button7.setLayoutX(560);
		button7.setLayoutY(180);

		Button button8 = new Button("8");
		button8.setPrefSize(40, 40);
		button8.setLayoutX(560);
		button8.setLayoutY(250);

		Button insertMoney = new Button("Input Money");
		insertMoney.setPrefSize(150, 40);
		insertMoney.setLayoutX(230);
		insertMoney.setLayoutY(350);
		insertMoney.setDisable(true);

		Button off = new Button("OFF");
		off.setPrefSize(60, 40);
		off.setLayoutX(560);
		off.setLayoutY(750);

		StackPane stack = new StackPane();
		stack.getChildren().addAll(screen, displayText, displayTextLeft, displayTextRight);
		StackPane.setAlignment(displayTextLeft, Pos.TOP_LEFT); // https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm
		StackPane.setAlignment(displayTextRight, Pos.TOP_RIGHT); // https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm
		stack.setLayoutX(55);
		stack.setLayoutY(20);

		layout.getChildren().add(stack);
		layout.getChildren().add(button1);
		layout.getChildren().add(button2);
		layout.getChildren().add(button3);
		layout.getChildren().add(button4);
		layout.getChildren().add(button5);
		layout.getChildren().add(button6);
		layout.getChildren().add(button7);
		layout.getChildren().add(button8);
		layout.getChildren().add(insertMoney);
		layout.getChildren().add(off);


		screen.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("Clicked the screen");
			}
		});

		button1.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("Clicked button1");
				int selection = 0;
				if (Controls.drinks.get(selection).IsEnough()) {
					insertMoney.setDisable(false);
				}
			}
		});
		button2.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("Clicked button2");
			}
		});
		button3.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("Clicked button3");
			}
		});
		button4.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("Clicked button4");
			}
		});
		button5.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("Clicked button5");
			}
		});
		button6.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("button6");
			}
		});
		button7.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("button7");
			}
		});
		button8.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				displayText.setText("button 8");
			}
		});
		insertMoney.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				moneyInput(stage);// input money.
			}
		});
		off.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				try {
					Controls.shutdown();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				stage.close();
			}
		});

		stage.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			if (event.getCode() == KeyCode.LEFT) {
				displayText.setText("Refilling");
				IngredientsReadWrite.refill();
			}
		});

		Scene scene = new Scene(layout, width, height);
		stage.setScene(scene);
		stage.show();

	}

	private void moneyInput(Stage stage) {
		Stage stage2 = new Stage();
		int width = 300;
		int height = 300;
		Pane layout2 = new Pane();
		double ret = 0;

		stage2.setTitle("money Input");// Titel
		TextArea line = new TextArea("Insert money");
		line.setPrefSize(150, 50);
		line.setLayoutX((width - 150) / 2);
		line.setLayoutY(50);

		Button done = new Button("Done");
		done.setPrefSize(100, 40);
		done.setLayoutX(100);
		done.setLayoutY(200);

		layout2.getChildren().addAll(line, done);
		
		done.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Double.parseDouble(line.getText());
				stage2.close();
			}
		});
		

		Scene scene2 = new Scene(layout2, width, height);
		stage2.setScene(scene2);
		stage2.show();

	}

}