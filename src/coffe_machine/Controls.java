package coffe_machine;

import java.io.IOException;
import java.util.ArrayList;

class Controls {

	public static int log[] = new int[2];
	public static ArrayList<HotDrink> drinks = new ArrayList<>();
	public static double milk = 0;
	public static double sugar = 0;
	public static double coffee = 0;
	public static double water = 0;
	public static double cocoa = 0;
	public static int cleaningInterval = 150;
	public static boolean needsCleaning;
	static boolean debug = false; // debbuging and writing errors to console.

	public static void main(String[] args) {
		if (debug) {
			try {
				startUp();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static boolean startUp() throws IOException {
		// Read the log and check if cleaning is needed
		LogReadWrite.read(log);
		// Read the status of ingredients. MUST READ BEFORE CREATING DRINKS
		IngredientsReadWrite.read();

		// read the recipies and create the drinks array
		CreateDrinks.read();
		needsCleaning();
		return true;
	}

	public static boolean shutdown() throws IOException {
		// salvesta logi všlisesse faili
		LogReadWrite.write(log);
		IngredientsReadWrite.write();
		return true;
	}

	public static boolean needsCleaning() {
		if (log[1] <= cleaningInterval) {
			if (Controls.debug) {
				System.out.println("#needsCleaning ERROR: Needs cleaning!");
			}
			needsCleaning = false;
			return true;
		} else {
			if (Controls.debug) {
				System.out.println("#needsCleaning does not need cleaning!");
			}
			needsCleaning = true;
			return false;
		}
	}
	
	public static boolean dispenseDrink(HotDrink drink) {
		if (Controls.debug) {
			System.out.println("#dispenseDrink started");
		}
		boolean test = drink.dispense();
		if (test) {
			log[0] = log[0] + 1;
			log[1] = log[1] + 1;
			needsCleaning();
			if (Controls.debug) {
				System.out.println("#dispenseDrink OK");
			}
			return true;
		} else {
			if (Controls.debug) {
				System.out.println("#dispenseDrink failed");
			}
			// Drink not available any more what to do, send info to
			return false;
		}

	}
}
