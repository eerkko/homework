package coffe_machine;

public class HotDrink {
	private String name;
	private double milk, sugar, coffee, water, cocoa, price;
	private boolean available;

	/**
	 * Constructor for making a new hot drink, input the parameters below to
	 * construct it.
	 * 
	 * @param name
	 *            input drink name (string)
	 * @param milk
	 *            input the amount of milk needed in ml (double)
	 * @param sugar
	 *            input the amount of sugar needed in g (double)
	 * @param coffee
	 *            input the amount of coffee beans needed in g (double)
	 * @param water
	 *            input the amount of water needed in ml (double)
	 * @param cocoa
	 *            input the amount of cocoa powder needed in g (double)
	 * @param price
	 *            input the price of the drink in � (double)
	 */
	public HotDrink(String name, double milk, double sugar, double coffee, double water, double cocoa, double price) {
		this.name = name;
		this.milk = milk;
		this.sugar = sugar;
		this.coffee = coffee;
		this.water = water;
		this.cocoa = cocoa;
		this.price = price;
		IsEnough();
	}

	public boolean IsEnough() {
		if (Controls.debug) {
			System.out.println("#HotDrink.isEnough Started");
		}
		if (this.milk > Controls.milk)
			this.available = false;
		else if (this.sugar > Controls.sugar)
			this.available = false;
		else if (this.coffee > Controls.coffee)
			this.available = false;
		else if (this.water > Controls.water)
			this.available = false;
		else if (this.cocoa > Controls.cocoa)
			this.available = false;
		else
			this.available = true;

		if (Controls.debug) {
			System.out.println("#HotDrink.isEnough OK " + this.available);
		}
		return this.available;
	}

	public String toString() {
		IsEnough();
		if (Controls.debug) {
			return "		#N: " + name + " P: " + price + "�  M:" + milk + " COf: " + coffee + " Coc:" + cocoa
					+ " S: " + sugar + " W: " + water + "		" + available;
		} else if (available)
			return name + " " + price;
		else
			return name + " OUT!";
	}

	// will duble check if the drink is available and if is wil use the
	// ingridients.
	public boolean dispense() {
		if (Controls.debug) {
			System.out.println("#HotDrink.dispense Started");
		}
		if (IsEnough()) {
			Controls.milk = Controls.milk - this.milk;
			Controls.sugar = Controls.sugar - this.sugar;
			Controls.coffee = Controls.coffee - this.coffee;
			Controls.water = Controls.water - this.water;
			Controls.cocoa = Controls.cocoa - this.cocoa;
			if (Controls.debug) {
				System.out.println("#HotDrink.dispense OK");
			}
			return true;
		} else {
			if (Controls.debug) {
				System.out.println("#HotDrink.dispense OK");
			}
			return false;
		}
	}

	public String getName() {
		return name;
	}

	public Double getPrice() {
		return price;
	}

}
