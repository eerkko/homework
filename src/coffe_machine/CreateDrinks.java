package coffe_machine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * 
 * @author Erko Ensling
 *	Creates the array of drinks, reads recipes from external file and fills the array
 *	.debug() will print out all the lines read and also the drinks created, Controls.debug needs to be true
 */
public class CreateDrinks {
	static String file = "C:\\Users\\Erko Ensling\\workspace\\Homework\\src\\coffe_machine\\recipies.txt";

	public static void main(String[] args) {
		if (Controls.debug) {
			debug();
		}
	}

	private static void debug() {
		// Test for reading and a file
		try {
			read();
		} catch (IOException e) {
			System.out.println("Reading failed" + file);
			e.printStackTrace();
		}
		for (int i = 0; i < Controls.drinks.size(); i++) {
			System.out.println(Controls.drinks.get(i));
		}

	}

	public static void read() throws IOException {
		if (Controls.debug) {
			System.out.println("#CreatDrinks.read started...");
		}
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		String tempName;
		double tempRecipie[] = { 0, 0, 0, 0, 0, 0 };

		while ((line = br.readLine()) != null) {

			if (line.equals("<drink>")) {

				tempName = "";
				for (int i = 0; i < tempRecipie.length; i++)
					tempRecipie[i] = 0;
				if (Controls.debug) {
					System.out.println("# LEIDSIN JOOGI");// debug
				}
				while (!(line = br.readLine()).equals("</drink>")) {
					// tempRecipie milk, sugar, coffee, water, cocoa, price

					if (line.equals("<name>")) {
						while ((line = br.readLine()) != null) {
							if (line.equals("</name>"))
								break;
							if (Controls.debug) {
								System.out.println("		#name:" + line); // test
								// line
								// to
							} // see if it
							// gets, the
							// right value.
							tempName = line;
						}
					}

					if (line.equals("<milk>")) {
						while ((line = br.readLine()) != null) {
							if (line.equals("</milk>"))
								break;
							if (Controls.debug) {
								System.out.println("		#Milk:" + line); // test
								// line
								// to
							} // see if it
							// gets, the
							// right value.
							tempRecipie[0] = Double.parseDouble(line);
						}
					}
					if (line.equals("<sugar>")) {
						while ((line = br.readLine()) != null) {
							if (line.equals("</sugar>"))
								break;
							if (Controls.debug) {
								System.out.println("		#sugar:" + line); // test
								// line
								// to
							} // see if it
							// gets, the
							// right value.
							tempRecipie[1] = Double.parseDouble(line);
						}
					}

					if (line.equals("<coffee>")) {
						while ((line = br.readLine()) != null) {
							if (line.equals("</coffee>"))
								break;
							if (Controls.debug) {
								System.out.println("		#coffee:" + line); // test
								// line
								// to
							} // see if it
							// gets, the
							// right value.
							tempRecipie[2] = Double.parseDouble(line);
						}
					}

					if (line.equals("<water>")) {
						while ((line = br.readLine()) != null) {
							if (line.equals("</water>"))
								break;
							if (Controls.debug) {
								System.out.println("		#water:" + line); // test
								// line
								// to
							} // see if it
							// gets, the
							// right value.
							tempRecipie[3] = Double.parseDouble(line);
						}
					}

					if (line.equals("<cocoa>")) {
						while ((line = br.readLine()) != null) {
							if (line.equals("</cocoa>"))
								break;
							if (Controls.debug) {
								System.out.println("		#cocoa:" + line); // test
								// line
								// to
							} // see if it
							// gets, the
							// right value.
							tempRecipie[4] = Double.parseDouble(line);
						}
					}

					if (line.equals("<price>")) {
						while ((line = br.readLine()) != null) {
							if (line.equals("</price>"))
								break;
							if (Controls.debug) {
								System.out.println("		#price:" + line); // test
								// line
								// to
							} // see if it
							// gets, the
							// right value.
							tempRecipie[5] = Double.parseDouble(line);
						}
					}
				}

				addToArray(tempName, tempRecipie);

			}
		}

		br.close();
		if (Controls.debug) {
			System.out.println("#CreatDrinks.read OK!");
		}
	}

	private static void addToArray(String name, double tmp[]) {
		HotDrink drink = new HotDrink(name, tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);// name,
		// milk,
		// sugar,
		// coffee,
		// water,
		// cocoa,
		// price
		Controls.drinks.add(drink);
		if (Controls.debug) {
			System.out.println("#CreateDrinks.addToArray added a new drink    " + drink);
		}
	}

}
