package coffe_machine;

import java.io.IOException;
import lib.TextIO;


/* Logic
 * Startup, system check:
 * 	Maintenance?
 * 	Everything enough, (check milk, coffee beans, etc) 
 * 	Cleaning?? (after XX times used need cleaning, save usage log to external file, and check it every time startup)
 * 1. Insert coin
 * 	Check if enough
 * 2. Select drink, or should it be before? 1.
 * 3. Dispense drink
 * 4. Remove ingredients from 
 *
 */
public class TextBasedGUI {
	public static void main(String[] args) throws InterruptedException, IOException {

		if (Controls.startUp()) { // will run the startup process and see if starting up worked
			drawDots("Starting UP. Coffe Machine - Erko Ensling IA17");

		}

		while (!Controls.needsCleaning) {
			emptySpace("");
			int i = 0;
			for (HotDrink s : Controls.drinks) {
				System.out.println("Drink " + (i+1) +". :" + s);
				i++;
			}
			int selection;
			System.out.print("Select your drink 1-" + i + " (Insert '0' to exit):");
			selection = TextIO.getInt() - 1; // array list starts with 0, need
			// to remove one from input.
			if (selection == -1) {
				break; // selected was '0' so exit
			} else if (Controls.drinks.get(selection).IsEnough()) {
				double money_input;
				System.out.print("Insert money:");
				money_input = TextIO.getDouble();
				if (money_input >= Controls.drinks.get(selection).getPrice()) {
					// enough money give a drink
					if (Controls.dispenseDrink(Controls.drinks.get(selection))) {
						// drink is available
						if (money_input > Controls.drinks.get(selection).getPrice()) {
							drawDots("Returning " + (money_input - Controls.drinks.get(selection).getPrice()));
						}
						drawACup();
					} else {
						System.out.println("Sorry this drink is not available at the moment!");
					}

				} else {
					drawDots("Not enough money, try again. Returning money.");
				}

			} else {
				System.out.println("Sorry this drink is not available at the moment!");
			}
			Thread.sleep(1500);
		} // end of while
		if (Controls.needsCleaning) {
			drawDots("Needs cleaning will shutdown");
		}
		Controls.shutdown();

		drawDots("Switching off");
	}

	private static void emptySpace(String text) {
		if (Controls.debug) {
			System.out.println("------" + text);
		} else {
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + text);
		}

	}

	private static void drawDots(String text) throws InterruptedException {
		emptySpace(text);
		for (int i = 0; i < 20; i++) {
			System.out.print(".");
			Thread.sleep(150);
		}
	}

	private static void drawACup() throws InterruptedException {
		drawDots(" Warming up");
		emptySpace("");
		System.out.println("    \n  \n  \n\\   /\n \\_/ \n");
		Thread.sleep(500);

		emptySpace("");
		System.out.println("    \n  \n  \n\\   /\n \\@/ \n");
		Thread.sleep(500);

		emptySpace("");
		System.out.println("    \n  \n  \n\\@@@/\n \\@/ \n");
		Thread.sleep(500);

		emptySpace("");
		System.out.println("Take your drink!\n\n");
		System.out.println("  ~  \n ~~ \n ~~~ \n\\@@@/\n \\@/ \n");
		Thread.sleep(1500);

	}

}
