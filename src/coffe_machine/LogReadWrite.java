package coffe_machine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//File Writer base Copied from  http://www.tutorialspoint.com/java/java_filewriter_class.htm
//File reader base copied from http://stackoverflow.com/questions/5868369/how-to-read-a-large-text-file-line-by-line-using-java
/**
 * @author Erko Ensling
 *	Reads external log file and saves logs to external file.
 */
public class LogReadWrite {
	static String file = "C:\\Users\\Erko Ensling\\workspace\\Homework\\src\\coffe_machine\\log.txt";

	public static void main(String[] args) {
		if (Controls.debug) {
			test();
		}
	}

	private static void test() {
		// Test for reading and writing a log file
		int test[] = { 10000, 200 };

		try {
			write(test);
		} catch (IOException e) {
			System.out.println("ERROR WRITING LOG");
			e.printStackTrace();
		}

		test[0] = 0;
		test[1] = 0;

		try {
			read(test);
		} catch (IOException e) {
			System.out.println("ERROR READING LOG");
			e.printStackTrace();
		}
		System.out.println("\n");
		for (int i = 0; i < test.length; i++) {
			System.out.println(test[i]);
		}
	}

	public static void write(int log[]) throws IOException {
		if (Controls.debug) {
			System.out.println("\n\n#Writing Log start!");
		}
		// creates a FileWriter Object
		FileWriter writer = new FileWriter(file);
		// Writes the content to the file, with given syntax

		writer.write("<total>\n" + log[0] + "\n</total>\n<since_clean>\n" + log[1] + "\n</since_clean>");
		writer.flush();
		writer.close();
		if (Controls.debug) {
			System.out.println("#Writing LOG ok!");
		}
	}

	public static void read(int log[]) throws IOException {
		if (Controls.debug) {
			System.out.println("\n\n#Reading Log start!");
		}

		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			if (line.equals("<total>")) {
				while ((line = br.readLine()) != null) {
					if (line.equals("</total>"))
						break;
					if (Controls.debug) {
						System.out.println("#LogRead.Write.read total value:" + line);
					}
					log[0] = Integer.parseInt(line);
				}
			}
			if (line.equals("<since_clean>")) {
				while ((line = br.readLine()) != null) {
					if (line.equals("</since_clean>"))
						break;
					if (Controls.debug) {
						System.out.println("#LogRead.Write.read since clean value:" + line);
					}
					log[1] = Integer.parseInt(line);
				}
			}
		}

		br.close();
		if (Controls.debug) {
			System.out.println("#LogRead.Write.read OK!");
		}

	}
}
